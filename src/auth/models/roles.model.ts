export enum RoleEnum {
  ADMIN = 'admin',
  CUSTOMER = 'customer',
  OWNER = 'owner',
  SUPER_ADMIN = 'super_admin',
}
