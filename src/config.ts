import { registerAs } from '@nestjs/config';
import { join } from 'path';

export default registerAs('config', () => {
  return {
    postgres: {
      host: process.env.PG_HOST,
      port: parseInt(process.env.PG_PORT, 10),
      username: process.env.PG_USER,
      password: process.env.PG_PASSWORD,
      database: process.env.PG_DB,
      synchronize: false,
      logging: false,
      entities: [join(__dirname, './**/*.entity{.ts,.js}')],
      migrations: [join(__dirname, './**/migrations/*{.ts,.js}')],
      migrationsTableName: 'migrations',
    },
    jwt: {
      expiration: process.env.ACCESS_TOKEN_EXPIRATION,
      refreshExpiration: process.env.REFRESH_TOKEN_EXPIRATION,
      refreshSecret: process.env.REFRESH_TOKEN_SECRET,
      secret: process.env.ACCESS_TOKEN_SECRET,
    },
    facebook: {
      callbackURL: process.env.FACEBOOK_CALLBACK_URL,
      clientID: process.env.FACEBOOK_CLIENT_ID,
      clientSecret: process.env.FACEBOOK_CLIENT_SECRET,
    },
  };
});
