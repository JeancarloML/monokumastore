import { IsString, IsUrl, IsNotEmpty } from 'class-validator';
import { PartialType, ApiProperty } from '@nestjs/swagger';

export class CreateBrandDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    description: 'The name of Brand',
    default: 'Brand' + Math.floor(Math.random() * 100),
  })
  readonly name: string;

  @IsUrl()
  @IsNotEmpty()
  @ApiProperty({
    description: 'The image of Brand',
    default: 'https://picsum.photos/200/300/' + Math.floor(Math.random() * 100),
  })
  readonly image: string;
}

export class UpdateBrandDto extends PartialType(CreateBrandDto) {}
