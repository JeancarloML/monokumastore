import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import { ApiTags, ApiOperation } from '@nestjs/swagger';

import { CreateProductDto, UpdateProductDto, FilterProductDto } from '../dtos';
import { JwtAuthGuard, RolesGuard } from '../../auth/guards';
import { ProductsService } from './../services';
import { Public, Roles } from 'src/auth/decorators';
import { RoleEnum } from '../../auth/models';

@UseGuards(JwtAuthGuard, RolesGuard)
@ApiTags('Products')
@Controller('products')
export class ProductsController {
  constructor(private productsService: ProductsService) {}

  @Public()
  @ApiOperation({ summary: 'List of products' })
  @Get()
  getProducts(@Query() params: FilterProductDto) {
    return this.productsService.findAll(params);
  }

  @Public()
  @ApiOperation({ summary: 'Find one product' })
  @Get(':productId')
  getOne(@Param('productId', ParseIntPipe) productId: number) {
    return this.productsService.findOne(productId);
  }

  @Roles(RoleEnum.ADMIN)
  @ApiOperation({ summary: 'Create product, required admin role' })
  @Post()
  create(@Body() payload: CreateProductDto) {
    return this.productsService.create(payload);
  }

  @Roles(RoleEnum.ADMIN)
  @ApiOperation({ summary: 'Update product, required admin role' })
  @Put(':id')
  update(
    @Param('id', ParseIntPipe) id: number,
    @Body() payload: UpdateProductDto,
  ) {
    return this.productsService.update(id, payload);
  }

  @Roles(RoleEnum.ADMIN)
  @ApiOperation({ summary: 'Add category to product, required admin role' })
  @Put(':id/category/:categoryId')
  addCategoryToProduct(
    @Param('id', ParseIntPipe) id: number,
    @Param('categoryId', ParseIntPipe) categoryId: number,
  ) {
    return this.productsService.addCategoryToProduct(id, categoryId);
  }

  @Roles(RoleEnum.ADMIN)
  @ApiOperation({ summary: 'Delete product, required admin role' })
  @Delete(':id')
  delete(@Param('id', ParseIntPipe) id: number) {
    return this.productsService.remove(id);
  }

  @Roles(RoleEnum.ADMIN)
  @ApiOperation({
    summary: 'Delete category from product, required admin role',
  })
  @Delete(':id/category/:categoryId')
  deleteCategory(
    @Param('id', ParseIntPipe) id: number,
    @Param('categoryId', ParseIntPipe) categoryId: number,
  ) {
    return this.productsService.removeCategoryByProduct(id, categoryId);
  }
}
