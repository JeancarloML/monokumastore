import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { ApiTags, ApiOperation } from '@nestjs/swagger';

import { BrandsService } from '../services';
import { CreateBrandDto, UpdateBrandDto } from '../dtos';
import { JwtAuthGuard, RolesGuard } from '../../auth/guards';
import { Public, Roles } from '../../auth/decorators';
import { RoleEnum } from '../../auth/models';

@UseGuards(JwtAuthGuard, RolesGuard)
@ApiTags('Brands')
@Controller('brands')
export class BrandsController {
  constructor(private brandsService: BrandsService) {}

  @Public()
  @ApiOperation({ summary: 'Find all brands' })
  @Get()
  findAll() {
    return this.brandsService.findAll();
  }

  @Public()
  @ApiOperation({ summary: 'Find a brand by id and show related products' })
  @Get(':id')
  get(@Param('id', ParseIntPipe) id: number) {
    return this.brandsService.findOne(id);
  }

  @Roles(RoleEnum.ADMIN)
  @ApiOperation({ summary: 'Create a brand, required admin role' })
  @Post()
  create(@Body() payload: CreateBrandDto) {
    return this.brandsService.create(payload);
  }

  @Roles(RoleEnum.ADMIN)
  @ApiOperation({ summary: 'Update a brand, required admin role' })
  @Put(':id')
  update(
    @Param('id', ParseIntPipe) id: number,
    @Body() payload: UpdateBrandDto,
  ) {
    return this.brandsService.update(id, payload);
  }

  @Roles(RoleEnum.ADMIN)
  @ApiOperation({ summary: 'Delete a brand, required admin role' })
  @Delete(':id')
  remove(@Param('id', ParseIntPipe) id: number) {
    return this.brandsService.remove(id);
  }
}
