import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { Between, FindOptionsWhere, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

import {
  CreateProductDto,
  FilterProductDto,
  UpdateProductDto,
} from './../dtos';
import { Product, Category, Brand } from '../../database/entities/products';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Product)
    private readonly productRepo: Repository<Product>,
    @InjectRepository(Brand)
    private readonly brandRepo: Repository<Brand>,
    @InjectRepository(Category)
    private readonly categoryRepo: Repository<Category>,
  ) {}

  async findAll(params?: FilterProductDto) {
    if (!params) {
      const products = await this.productRepo.find({
        relations: ['brand'],
      });
      if (products.length === 0) {
        throw new NotFoundException('Products not found');
      }
      return products;
    }
    const where: FindOptionsWhere<Product> = {};
    const { limit, offset, maxPrice, minPrice } = params;
    if (minPrice && maxPrice) where.price = Between(minPrice, maxPrice);
    const products = await this.productRepo.find({
      relations: ['brand'],
      take: limit,
      skip: offset,
      where,
    });
    if (products.length === 0) {
      throw new NotFoundException('Products not found');
    }
    return products;
  }

  async findOne(id: number) {
    const product = await this.productRepo.findOne({
      where: { id },
      relations: ['brand', 'categories'],
    });
    if (!product) throw new NotFoundException(`Product #${id} not found`);
    return product;
  }

  async create(data: CreateProductDto) {
    const newProduct = this.productRepo.create(data);
    await this.validateNameUnique(newProduct.name);
    if (data.brandId) newProduct.brand = await this.validateBrand(data.brandId);
    if (data.categoryIds) {
      newProduct.categories = await this.validateCategories(data.categoryIds);
    }
    return this.productRepo.save(newProduct);
  }

  async update(id: number, changes: UpdateProductDto) {
    const product = await this.validateNotFound(id);
    if (changes.name) await this.validateNameUnique(changes.name);
    if (changes.brandId) {
      product.brand = await this.validateBrand(changes.brandId);
    }
    if (changes.categoryIds) {
      product.categories = await this.validateCategories(changes.categoryIds);
    }
    this.productRepo.merge(product, changes);
    return this.productRepo.save(product);
  }

  async removeCategoryByProduct(productId: number, categoryId: number) {
    const product = await this.validateNotFound(productId);
    const category = await this.validateCategory(categoryId);
    product.categories = product.categories.filter(
      (category) => category.id !== categoryId,
    );
    return this.productRepo.save(product);
  }

  async addCategoryToProduct(productId: number, categoryId: number) {
    const product = await this.validateNotFound(productId);
    const category = await this.validateCategory(categoryId);
    if (product.categories.find((category) => category.id === categoryId)) {
      return this.productRepo.save(product);
    }
    product.categories.push(category);
    return this.productRepo.save(product);
  }

  async remove(id: number) {
    const product = await this.validateNotFound(id);
    return this.productRepo.remove(product);
  }

  private async validateNotFound(id: number) {
    const product = await this.productRepo.findOne({
      where: { id },
      relations: ['brand', 'categories'],
    });
    if (!product) throw new NotFoundException(`Product #${id} not found`);
    return product;
  }

  private async validateNameUnique(name: string) {
    const product = await this.productRepo.findOne({ where: { name } });
    if (product) throw new BadRequestException('Product already exists');
  }

  private async validateBrand(brandId: number) {
    const brand = await this.brandRepo.findOne({
      where: { id: brandId },
    });
    if (!brand) throw new NotFoundException('Brand not found');
    return brand;
  }

  private async validateCategories(categoryIds: number[]) {
    const categories = await this.categoryRepo.findByIds(categoryIds);
    if (categories.length !== categoryIds.length) {
      throw new BadRequestException('One or more categories not found');
    }
    return categories;
  }

  private async validateCategory(categoryId: number) {
    const category = await this.categoryRepo.findOne({
      where: { id: categoryId },
    });
    if (!category) throw new NotFoundException('Category not found');
    return category;
  }
}
