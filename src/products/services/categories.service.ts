import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { Category } from '../../database/entities/products';
import { CreateCategoryDto, UpdateCategoryDto } from '../dtos';

@Injectable()
export class CategoriesService {
  constructor(
    @InjectRepository(Category)
    private readonly categoriesRepo: Repository<Category>,
  ) {}

  async findAll() {
    const categories = await this.categoriesRepo.find();
    if (categories.length === 0) {
      throw new NotFoundException('Categories not found');
    }
    return categories;
  }

  async findOne(id: number) {
    const category = await this.categoriesRepo.findOne({
      where: { id },
    });
    if (!category) throw new NotFoundException(`Category #${id} not found`);
    return category;
  }

  async create(data: CreateCategoryDto) {
    await this.validateNameUnique(data.name);
    const newCategory = this.categoriesRepo.create(data);
    return this.categoriesRepo.save(newCategory);
  }

  async update(id: number, changes: UpdateCategoryDto) {
    const category = await this.validateNotFound(id);
    if (changes.name) await this.validateNameUnique(changes.name);
    const updatedCategory = this.categoriesRepo.merge(category, changes);
    return this.categoriesRepo.save(updatedCategory);
  }

  async remove(id: number) {
    const category = await this.validateNotFound(id);
    return this.categoriesRepo.remove(category);
  }

  private async validateNotFound(id: number) {
    const category = await this.categoriesRepo.findOne({
      where: { id },
    });
    if (!category) throw new NotFoundException(`Category #${id} not found`);
    return category;
  }

  private async validateNameUnique(name: string) {
    const categoryWithName = await this.categoriesRepo.findOne({
      where: { name },
    });
    if (categoryWithName) {
      throw new NotFoundException(`Category with name ${name} already exists`);
    }
  }
}
