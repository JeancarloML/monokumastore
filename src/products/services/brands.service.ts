import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { Brand } from '../../database/entities/products';
import { CreateBrandDto, UpdateBrandDto } from '../dtos';

@Injectable()
export class BrandsService {
  constructor(
    @InjectRepository(Brand) private readonly brandsRepo: Repository<Brand>,
  ) {}

  async findAll() {
    const brands = await this.brandsRepo.find();
    if (brands.length === 0) throw new NotFoundException('Brands not found');
    return brands;
  }

  async findOne(id: number) {
    const brand = await this.brandsRepo.findOne({
      where: { id },
      relations: ['products'],
    });
    if (!brand) throw new NotFoundException(`Brand #${id} not found`);
    return brand;
  }

  async create(data: CreateBrandDto) {
    await this.validateNameUnique(data.name);
    const newBrand = this.brandsRepo.create(data);
    return this.brandsRepo.save(newBrand);
  }

  async update(id: number, changes: UpdateBrandDto) {
    const brand = await this.validateNotFound(id);
    if (changes.name) await this.validateNameUnique(changes.name);
    this.brandsRepo.merge(brand, changes);
    return this.brandsRepo.save(brand);
  }

  async remove(id: number) {
    const brand = await this.validateNotFound(id);
    return this.brandsRepo.remove(brand);
  }

  private async validateNotFound(id: number) {
    const brand = await this.brandsRepo.findOne({
      where: { id },
    });
    if (!brand) throw new NotFoundException(`Brand #${id} not found`);
    return brand;
  }

  private async validateNameUnique(name: string) {
    const brandWithName = await this.brandsRepo.findOne({
      where: { name },
    });
    if (brandWithName) {
      throw new NotFoundException(`Brand with name ${name} already exists`);
    }
  }
}
