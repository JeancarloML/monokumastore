import { Module } from '@nestjs/common';

import { AuthModule } from '../auth/auth.module';
import {
  BrandsController,
  CategoriesController,
  ProductsController,
} from './controllers';
import { ProductsService, CategoriesService, BrandsService } from './services';

@Module({
  controllers: [ProductsController, CategoriesController, BrandsController],
  exports: [],
  imports: [AuthModule],
  providers: [ProductsService, CategoriesService, BrandsService],
})
export class ProductsModule {}
