import { ConfigType } from '@nestjs/config';
import { Module, Global } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { Product, Brand, Category } from './entities/products';
import config from 'src/config';
import {
  Customer,
  Order,
  OrderItem,
  Role,
  Source,
  User,
} from './entities/users';
import { getTypeOrmModuleOptions } from './data-source';

@Global()
@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      inject: [config.KEY],
      useFactory: async ({ postgres }: ConfigType<typeof config>) => ({
        ...getTypeOrmModuleOptions(),
      }),
    }),
    TypeOrmModule.forFeature([
      Brand,
      Category,
      Customer,
      Order,
      OrderItem,
      Product,
      Role,
      Source,
      User,
    ]),
  ],
  exports: [TypeOrmModule],
})
export class DatabaseModule {}
