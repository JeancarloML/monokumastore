import {MigrationInterface, QueryRunner} from "typeorm";

export class updatemodelCustomer1653587790257 implements MigrationInterface {
    name = 'updatemodelCustomer1653587790257'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "customers" RENAME COLUMN "lastName" TO "lastname"`);
        await queryRunner.query(`CREATE TYPE "public"."source_name_enum" AS ENUM('facebook', 'google', 'email', 'twitter', 'instagram', 'microsoft')`);
        await queryRunner.query(`CREATE TABLE "source" ("created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "id" SERIAL NOT NULL, "name" "public"."source_name_enum" NOT NULL DEFAULT 'email', CONSTRAINT "UQ_e64619e22dc97d6525c86ef8af5" UNIQUE ("name"), CONSTRAINT "PK_018c433f8264b58c86363eaadde" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "users" ADD "source_id" integer`);
        await queryRunner.query(`ALTER TABLE "users" ADD CONSTRAINT "FK_6b129e2863f0568bf8592bb3b0b" FOREIGN KEY ("source_id") REFERENCES "source"("id") ON DELETE SET NULL ON UPDATE CASCADE`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "users" DROP CONSTRAINT "FK_6b129e2863f0568bf8592bb3b0b"`);
        await queryRunner.query(`ALTER TABLE "users" DROP COLUMN "source_id"`);
        await queryRunner.query(`DROP TABLE "source"`);
        await queryRunner.query(`DROP TYPE "public"."source_name_enum"`);
        await queryRunner.query(`ALTER TABLE "customers" RENAME COLUMN "lastname" TO "lastName"`);
    }

}
