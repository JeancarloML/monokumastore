import {MigrationInterface, QueryRunner} from "typeorm";

export class notUniqueName1652968816787 implements MigrationInterface {
    name = 'notUniqueName1652968816787'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "customers" DROP CONSTRAINT "UQ_b942d55b92ededa770041db9ded"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "customers" ADD CONSTRAINT "UQ_b942d55b92ededa770041db9ded" UNIQUE ("name")`);
    }

}
