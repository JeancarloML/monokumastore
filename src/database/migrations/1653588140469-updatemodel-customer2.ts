import {MigrationInterface, QueryRunner} from "typeorm";

export class updatemodelCustomer21653588140469 implements MigrationInterface {
    name = 'updatemodelCustomer21653588140469'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "customers" ALTER COLUMN "phone" DROP NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "customers" ALTER COLUMN "phone" SET NOT NULL`);
    }

}
