import {MigrationInterface, QueryRunner} from "typeorm";

export class initRta1652807270099 implements MigrationInterface {
    name = 'initRta1652807270099'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "users" ADD CONSTRAINT "UQ_97672ac88f789774dd47f7c8be3" UNIQUE ("email")`);
        await queryRunner.query(`ALTER TABLE "customers" DROP CONSTRAINT "UQ_8e11140e3639e6d35a9f79f9808"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "customers" ADD CONSTRAINT "UQ_8e11140e3639e6d35a9f79f9808" UNIQUE ("lastName")`);
        await queryRunner.query(`ALTER TABLE "users" DROP CONSTRAINT "UQ_97672ac88f789774dd47f7c8be3"`);
    }

}
