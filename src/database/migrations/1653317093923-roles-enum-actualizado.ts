import {MigrationInterface, QueryRunner} from "typeorm";

export class rolesEnumActualizado1653317093923 implements MigrationInterface {
    name = 'rolesEnumActualizado1653317093923'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TYPE "public"."roles_name_enum" RENAME TO "roles_name_enum_old"`);
        await queryRunner.query(`CREATE TYPE "public"."roles_name_enum" AS ENUM('customer', 'admin', 'owner', 'super_admin')`);
        await queryRunner.query(`ALTER TABLE "roles" ALTER COLUMN "name" DROP DEFAULT`);
        await queryRunner.query(`ALTER TABLE "roles" ALTER COLUMN "name" TYPE "public"."roles_name_enum" USING "name"::"text"::"public"."roles_name_enum"`);
        await queryRunner.query(`ALTER TABLE "roles" ALTER COLUMN "name" SET DEFAULT 'customer'`);
        await queryRunner.query(`DROP TYPE "public"."roles_name_enum_old"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TYPE "public"."roles_name_enum_old" AS ENUM('customer', 'admin')`);
        await queryRunner.query(`ALTER TABLE "roles" ALTER COLUMN "name" DROP DEFAULT`);
        await queryRunner.query(`ALTER TABLE "roles" ALTER COLUMN "name" TYPE "public"."roles_name_enum_old" USING "name"::"text"::"public"."roles_name_enum_old"`);
        await queryRunner.query(`ALTER TABLE "roles" ALTER COLUMN "name" SET DEFAULT 'customer'`);
        await queryRunner.query(`DROP TYPE "public"."roles_name_enum"`);
        await queryRunner.query(`ALTER TYPE "public"."roles_name_enum_old" RENAME TO "roles_name_enum"`);
    }

}
