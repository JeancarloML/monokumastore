import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';

import { DateAt } from '../../../common/entities';
import { Product } from '.';

@Entity({ name: 'brands' })
export class Brand extends DateAt {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 255, unique: true })
  name: string;

  @Column({ type: 'varchar', length: 255 })
  image: string;

  @OneToMany(() => Product, (product) => product.brand)
  products: Product[];
}
