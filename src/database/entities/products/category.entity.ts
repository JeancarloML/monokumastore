import { PrimaryGeneratedColumn, Column, Entity, ManyToMany } from 'typeorm';

import { DateAt } from '../../../common/entities';
import { Product } from '.';

@Entity({ name: 'categories' })
export class Category extends DateAt {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 255, unique: true })
  name: string;

  @ManyToMany(() => Product, (product) => product.categories)
  products: Product[];
}
