import { DataSource, DataSourceOptions } from 'typeorm';
import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { join } from 'path';

export const getTypeOrmModuleOptions = (): TypeOrmModuleOptions => {
  const dbOptions = {
    synchronize: false,
    autoLoadEntities: false,
    entities: [join(__dirname, '../**/*.entity{.ts,.js}')],
  };

  Object.assign(dbOptions, {
    type: 'postgres',
    host: process.env.PG_HOST || 'localhost',
    port: parseInt(process.env.PG_PORT, 10) || 5432,
    database: process.env.PG_DB || 'my_db',
    username: process.env.PG_USER || 'root',
    password: process.env.PG_PASSWORD || 'admin',
  });

  return dbOptions;
};

export const getDataSourceOptions = (): DataSourceOptions => {
  const options: DataSourceOptions = {
    ...getTypeOrmModuleOptions(),
  } as DataSourceOptions;

  Object.assign(options, {
    migrationsTableName: 'migrations',
    migrations: [join(__dirname, '../**/migrations/*{.ts,.js}')],
  } as Partial<DataSourceOptions>);

  console.log(options);
  return options;
};
export default new DataSource(getDataSourceOptions());
